assert('texture') do
  texture = SFML::Texture.create(25, 25)
  sprite = SFML::Sprite.new

  sprite.texture = texture
  assert_equal texture, sprite.texture
end
