assert('getters') do
  vector = SFML::Vector.new(1, 2)

  assert_equal 1, vector.x
  assert_equal 2, vector.y
  assert_equal nil, vector.z
end

assert('x=') do
  vector = SFML::Vector.new(0, 0)
  vector.x = 1
  assert_equal 1, vector.x
end

assert('y=') do
  vector = SFML::Vector.new(0, 0)
  vector.y = 1
  assert_equal 1, vector.y
end

assert('z=') do
  vector = SFML::Vector.new(0, 0, 0)
  vector.z = 1
  assert_equal 1, vector.z
end

# creates a new vector
assert('transform') do
  vector = SFML::Vector.new(1, 1)

  transform = SFML::Transform.new
  transform.translate(1, 2)
  new_vector = vector.transform transform

  assert_equal 2, new_vector.x
  assert_equal 3, new_vector.y
end
