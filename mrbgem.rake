MRuby::Gem::Specification.new('mruby-sfml') do |spec|
  spec.license = 'MIT'
  spec.authors = 'Tom Chipchase'

  csfml_dir = if ENV['OS'] == 'Windows_NT'
                if ENV['Platform'] == 'X64'
                  "#{dir}/csfml-2.1-64bit"
                else
                  "#{dir}/csfml-2.1-32bit"
                end
              end

  lib = if ENV['VisualStudioVersion'] || ENV['VSINSTALLDIR']
          'msvc'
        else
          'gcc'
        end

  spec.linker.libraries << %w(csfml-audio csfml-graphics csfml-window csfml-system)
  spec.cc.include_paths << "#{csfml_dir}/CSFML-2.1/include"
  spec.linker.library_paths << "#{csfml_dir}/CSFML-2.1/lib/#{lib}"
end
