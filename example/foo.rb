include SFML

video_mode = VideoMode.new(800, 600)
window = Window.new(video_mode, 'Foo')

texture = Texture.create_from_file('foo.bmp')

sprite = Sprite.new
sprite.texture = texture
origin = Vector.new(16, 16);
sprite.origin = origin

sprite2 = Sprite.new(sprite)

position  = Vector.new(100, 100)
sprite.position = position

sprite.rotation = 45.0
puts sprite.rotation
scale = Vector.new(2, 2);
sprite.scale = scale

offset = Vector.new(100, 200);

sprite2.move offset
sprite2.rotate 33

font = Font.create_from_file('arial.ttf')

text = Text.new
text.font = font
text.string = 'Hello, World!'
text.char_size = 50

music = Music.create_from_file('ACDC_-_Back_In_Black-sample.ogg')
# music.play

while window.open?

  while (event = window.poll_event)
    case event
    when SFML::Event::Closed
      window.close!
    end
  end

  window.clear!
  window.draw_sprite(sprite)
  window.draw_sprite(sprite2)
  window.draw_text(text)
  window.display!
end
