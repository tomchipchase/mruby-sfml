module SFML
  class Sprite
    # We need to keep a reference to texture so it doesn't get garbage
    # collected.
    def texture=(texture)
      @texture=texture
      set_texture(texture)
    end

    attr_reader :texture

    private :set_texture
  end
end
