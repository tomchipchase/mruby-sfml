module SFML
  class Window
    def draw(drawable)
      case drawable
      when SFML::Sprite then draw_sprite(drawable)
      when SFML::Text then draw_text(drawable)
      end
    end
  end
end
