#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>

#include <SFML/Graphics.h>

#include "mrb_sfml_transform.h"

static struct RClass *class_Vector;

static const struct mrb_data_type mrb_sfml_vector_type = { "SFML::Vector",
  mrb_free
};

typedef struct mrb_sfml_vector_data
{
  mrb_value x;
  mrb_value y;
  mrb_value z;
} mrb_sfml_vector_data;

mrb_sfml_vector_data *
mrb_get_sfml_vector (mrb_state * mrb, mrb_value value)
{
  return DATA_GET_PTR (mrb, value, &mrb_sfml_vector_type,
                       mrb_sfml_vector_data);
}

static mrb_value
wrap_vector (mrb_state * mrb, mrb_value x, mrb_value y, mrb_value z)
{
  mrb_sfml_vector_data *data;

  data = mrb_malloc (mrb, sizeof (*data));

  data->x = x;
  data->y = y;
  data->z = z;

  return mrb_obj_value (Data_Wrap_Struct (mrb, class_Vector,
                                          &mrb_sfml_vector_type, data));
}

static mrb_value
initialize (mrb_state * mrb, mrb_value self)
{
  mrb_value x, y, z;
  size_t argc;
  mrb_sfml_vector_data *data;

  data = mrb_malloc (mrb, sizeof (*data));

  DATA_TYPE (self) = &mrb_sfml_vector_type;

  argc = mrb_get_args (mrb, "oo|o", &x, &y, &z);

  data->x = x;
  data->y = y;

  switch (argc)
    {
    case (2):
      data->z = mrb_nil_value ();
      break;
    case (3):
      data->z = z;
      break;
    default:
      mrb_raise (mrb, E_ARGUMENT_ERROR, "wrong number of arguments");
      break;
    }

  DATA_PTR (self) = data;

  return self;
}

static mrb_value
set_x (mrb_state * mrb, mrb_value self)
{
  mrb_value arg1;
  mrb_sfml_vector_data *data;

  mrb_get_args (mrb, "o", &arg1);

  data = mrb_get_sfml_vector (mrb, self);

  data->x = arg1;

  return arg1;
}

static mrb_value
set_y (mrb_state * mrb, mrb_value self)
{
  mrb_value arg1;
  mrb_sfml_vector_data *data;

  mrb_get_args (mrb, "o", &arg1);

  data = mrb_get_sfml_vector (mrb, self);

  data->y = arg1;

  return arg1;
}

static mrb_value
set_z (mrb_state * mrb, mrb_value self)
{
  mrb_value arg1;
  mrb_sfml_vector_data *data;

  mrb_get_args (mrb, "o", &arg1);

  data = mrb_get_sfml_vector (mrb, self);

  data->z = arg1;

  return arg1;
}

static mrb_value
get_x (mrb_state * mrb, mrb_value self)
{
  mrb_sfml_vector_data *data;

  data = mrb_get_sfml_vector (mrb, self);
  return data->x;
}

static mrb_value
get_y (mrb_state * mrb, mrb_value self)
{
  mrb_sfml_vector_data *data;

  data = mrb_get_sfml_vector (mrb, self);
  return data->y;
}

static mrb_value
get_z (mrb_state * mrb, mrb_value self)
{
  mrb_sfml_vector_data *data;

  data = mrb_get_sfml_vector (mrb, self);
  return data->z;
}

static int
mrb_value_to_int (mrb_state * mrb, mrb_value val)
{
  switch (mrb_type (val))
    {
    case MRB_TT_FIXNUM:
      return (int) mrb_fixnum (val);
    case MRB_TT_FLOAT:
      return (int) mrb_float (val);
    default:
      mrb_raise (mrb, E_TYPE_ERROR, "Not a number");
      return 0;
    }
}

static unsigned int
mrb_value_to_unsigned_int (mrb_state * mrb, mrb_value val)
{
  switch (mrb_type (val))
    {
    case MRB_TT_FIXNUM:
      return (unsigned int) mrb_fixnum (val);
    case MRB_TT_FLOAT:
      return (unsigned int) mrb_float (val);
    default:
      mrb_raise (mrb, E_TYPE_ERROR, "Not a number");
      return 0;
    }
}

static float
mrb_value_to_float (mrb_state * mrb, mrb_value val)
{
  switch (mrb_type (val))
    {
    case MRB_TT_FIXNUM:
      return (float) mrb_fixnum (val);
    case MRB_TT_FLOAT:
      return (float) mrb_float (val);
    default:
      mrb_raise (mrb, E_TYPE_ERROR, "Not a number");
      return 0;
    }
}

sfVector2i
mrb_get_sfml_vector_2i (mrb_state * mrb, mrb_value self)
{
  mrb_sfml_vector_data *data;
  int x, y;

  data = mrb_get_sfml_vector (mrb, self);

  x = mrb_value_to_int (mrb, data->x);
  y = mrb_value_to_int (mrb, data->y);

  return (sfVector2i) { x, y };
}

mrb_value
wrap_vector_2i (mrb_state * mrb, sfVector2i vector)
{
  return wrap_vector (mrb, mrb_fixnum_value (vector.x),
                      mrb_fixnum_value (vector.y), mrb_nil_value ());
}

sfVector2u
mrb_get_sfml_vector_2u (mrb_state * mrb, mrb_value self)
{
  mrb_sfml_vector_data *data;
  unsigned int x, y;

  data = mrb_get_sfml_vector (mrb, self);

  x = mrb_value_to_unsigned_int (mrb, data->x);
  y = mrb_value_to_unsigned_int (mrb, data->y);

  return (sfVector2u) { x, y };
}

mrb_value
wrap_vector_2u (mrb_state * mrb, sfVector2u vector)
{
  return wrap_vector (mrb, mrb_fixnum_value (vector.x),
                      mrb_fixnum_value (vector.y), mrb_nil_value ());
}

sfVector2f
mrb_get_sfml_vector_2f (mrb_state * mrb, mrb_value self)
{
  mrb_sfml_vector_data *data;
  float x, y;

  data = mrb_get_sfml_vector (mrb, self);

  x = mrb_value_to_float (mrb, data->x);
  y = mrb_value_to_float (mrb, data->y);

  return (sfVector2f) { x, y };
}

mrb_value
wrap_vector_2f (mrb_state * mrb, sfVector2f vector)
{
  return wrap_vector (mrb, mrb_float_value (mrb, vector.x),
                      mrb_float_value (mrb, vector.y), mrb_nil_value ());
}

sfVector3f
mrb_get_sfml_vector_3f (mrb_state * mrb, mrb_value self)
{
  mrb_sfml_vector_data *data;
  float x, y, z;

  data = mrb_get_sfml_vector (mrb, self);

  x = mrb_value_to_float (mrb, data->x);
  y = mrb_value_to_float (mrb, data->y);
  z = mrb_value_to_float (mrb, data->z);

  return (sfVector3f) { x, y, z };
}

mrb_value
wrap_vector_3f (mrb_state * mrb, sfVector3f vector)
{
  return wrap_vector (mrb, mrb_float_value (mrb, vector.x),
                      mrb_float_value (mrb, vector.y),
                      mrb_float_value (mrb, vector.z));
}

static mrb_value
transform (mrb_state * mrb, mrb_value self)
{
  mrb_value arg1;
  sfTransform * transform;
  sfVector2f point, transformed_point;

  mrb_get_args (mrb, "o", &arg1);

  point = mrb_get_sfml_vector_2f (mrb, self);
  transform = mrb_get_sfml_transform (mrb, arg1);

  transformed_point = sfTransform_transformPoint(transform, point);

  return wrap_vector_2f (mrb, transformed_point);
}

void
mrb_mruby_sfml_vector_init (mrb_state * mrb, struct RClass *sfml)
{
  class_Vector =
    mrb_define_class_under (mrb, sfml, "Vector", mrb->object_class);

  MRB_SET_INSTANCE_TT (class_Vector, MRB_TT_DATA);

  mrb_define_method (mrb, class_Vector, "initialize", initialize,
                     MRB_ARGS_REQ (2));
  mrb_define_method (mrb, class_Vector, "x=", set_x, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Vector, "x", get_x, MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Vector, "y=", set_y, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Vector, "y", get_y, MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Vector, "z=", set_z, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Vector, "z", get_z, MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Vector, "transform", transform,
                     MRB_ARGS_REQ (1));
}
