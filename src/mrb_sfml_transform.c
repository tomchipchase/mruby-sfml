#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>

#include <SFML/Graphics.h>

static struct RClass *class_Transform;

static const struct mrb_data_type mrb_sfml_transform_type = { "SFML::Transform",
  mrb_free
};

static void
copy_transform_to_pointer(sfTransform transform, sfTransform * data)
{
  for (int i = 0; i < 9; i++)
  {
    data->matrix[i] = transform.matrix[i];
  }
}

sfTransform *
mrb_get_sfml_transform (mrb_state * mrb, mrb_value value)
{
  return DATA_GET_PTR (mrb, value, &mrb_sfml_transform_type, sfTransform);
}

static mrb_value
wrap_transform (mrb_state * mrb, sfTransform transform)
{
  sfTransform *data;

  data = mrb_malloc (mrb, sizeof (*data));
  copy_transform_to_pointer(transform, data);

  return mrb_obj_value (Data_Wrap_Struct (mrb, class_Transform,
                                          &mrb_sfml_transform_type, data));
}

static mrb_value
initialize (mrb_state * mrb, mrb_value self)
{
  mrb_float a00, a01, a02, a10, a11, a12, a20, a21, a22;
  sfTransform transform, *data;
  size_t argc;

  DATA_TYPE (self) = &mrb_sfml_transform_type;

  argc = mrb_get_args (mrb, "|fffffffff", &a00, &a01, &a02,
                                          &a10, &a11, &a12,
                                          &a20, &a21, &a22);

  data = mrb_malloc (mrb, sizeof (*data));

  switch(argc)
    {
    case (0):
      transform = sfTransform_Identity;
      break;
    case (9):
      transform = sfTransform_fromMatrix((float) a00, (float) a01, (float) a02,
                                         (float) a10, (float) a11, (float) a12,
                                         (float) a20, (float) a21, (float) a22);
      break;
    default:
      mrb_raise (mrb, E_ARGUMENT_ERROR, "wrong number of arguments (0 or 9)");
      break;
    }

  copy_transform_to_pointer(transform, data);

  DATA_PTR (self) = data;

  return self;
}

static mrb_value
inverse (mrb_state * mrb, mrb_value value)
{
  sfTransform *data, inverse;

  data = mrb_get_sfml_transform (mrb, value);

  inverse = sfTransform_getInverse (data);

  return wrap_transform (mrb, inverse);
}

// TODO: non-destructive combine? (create a new transform object)
static mrb_value
combine (mrb_state * mrb, mrb_value self)
{
  mrb_value arg1;
  sfTransform *data, *other;

  mrb_get_args (mrb, "o", &arg1);

  data = mrb_get_sfml_transform (mrb, self);
  other = mrb_get_sfml_transform(mrb, arg1);

  sfTransform_combine(data, other);

  return self;
}

static mrb_value
translate (mrb_state * mrb, mrb_value self)
{
  mrb_float x, y;
  sfTransform *data;

  data = mrb_get_sfml_transform (mrb, self);
  mrb_get_args (mrb, "ff", &x, &y);

  sfTransform_translate(data, (float) x, (float) y);

  return self;
}

static mrb_value
rotate (mrb_state * mrb, mrb_value self)
{
  mrb_float angle, x, y;
  sfTransform *data;
  size_t argc;

  data = mrb_get_sfml_transform (mrb, self);
  argc = mrb_get_args (mrb, "f|ff", &angle, &x, &y);

  switch(argc)
    {
    case (1):
      sfTransform_rotate(data, (float) angle);
      break;
    case (3):
      sfTransform_rotateWithCenter(data, (float) angle, (float) x, (float) y);
      break;
    default:
      mrb_raise (mrb, E_ARGUMENT_ERROR, "wrong number of arguments (1 or 3)");
      break;
    }

  return self;
}

static mrb_value
scale (mrb_state * mrb, mrb_value self)
{
  mrb_float scale_x, scale_y, center_x, center_y;
  sfTransform *data;
  size_t argc;

  data = mrb_get_sfml_transform (mrb, self);
  argc = mrb_get_args (mrb, "ff|ff", &scale_x, &scale_y, &center_x, &center_y);

  switch(argc)
    {
    case (2):
      sfTransform_scale(data, (float) scale_x, (float) scale_y);
      break;
    case (3):
      sfTransform_scaleWithCenter(data, (float) scale_x, (float) scale_y,
                                  (float) center_x, (float) center_y);
      break;
    default:
      mrb_raise (mrb, E_ARGUMENT_ERROR, "wrong number of arguments (2 or 4)");
      break;
    }

  return self;
}

void
mrb_mruby_sfml_transform_init (mrb_state * mrb, struct RClass *sfml)
{
  class_Transform = mrb_define_class_under (mrb, sfml, "Transform",
                                            mrb->object_class);

  mrb_define_method (mrb, class_Transform, "initialize", initialize,
                     MRB_ARGS_ANY());
  mrb_define_method (mrb, class_Transform, "inverse", inverse, MRB_ARGS_NONE());
  mrb_define_method (mrb, class_Transform, "+=", combine, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Transform, "translate", translate,
                     MRB_ARGS_REQ (2));
  mrb_define_method (mrb, class_Transform, "rotate", rotate, MRB_ARGS_ANY());
  mrb_define_method (mrb, class_Transform, "scale", scale, MRB_ARGS_ANY());

  MRB_SET_INSTANCE_TT (class_Transform, MRB_TT_DATA);
}
