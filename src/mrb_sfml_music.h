#ifndef _MRB_SFML_MUSIC_H_
#define _MRB_SFML_MUSIC_H_

#include <mruby.h>

#include <SFML/Audio.h>

void mrb_mruby_sfml_music_init (mrb_state *, struct RClass *);

sfMusic *mrb_get_sfml_music (mrb_state *, mrb_value);

#endif
