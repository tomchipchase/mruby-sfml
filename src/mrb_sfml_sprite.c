#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>

#include <SFML/Graphics.h>

#include "mrb_sfml_texture.h"
#include "mrb_sfml_vector.h"

static void
sprite_free (mrb_state * mrb, void *p)
{
  sfSprite_destroy (p);
}

static const struct mrb_data_type mrb_sfml_sprite_type = { "SFML::Sprite",
  sprite_free
};

sfSprite *
mrb_get_sfml_sprite (mrb_state * mrb, mrb_value value)
{
  return DATA_GET_PTR (mrb, value, &mrb_sfml_sprite_type, sfSprite);
}

static mrb_value
initialize (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  mrb_value arg;
  size_t argc;

  argc = mrb_get_args (mrb, "|o", &arg);

  switch (argc)
    {
    case (0):
      sprite = sfSprite_create ();
      break;
    case (1):
      sprite = sfSprite_copy (mrb_get_sfml_sprite (mrb, arg));
      break;
    default:
      mrb_raise (mrb, E_ARGUMENT_ERROR, "wrong number of arguments");
      break;
    }

  DATA_TYPE (self) = &mrb_sfml_sprite_type;
  DATA_PTR (self) = sprite;
  return self;
}

static mrb_value
set_texture (mrb_state * mrb, mrb_value self)
{
  mrb_value arg1;
  sfTexture *texture;
  sfSprite *sprite;

  mrb_get_args (mrb, "o", &arg1);

  texture = mrb_get_sfml_texture (mrb, arg1);
  sprite = mrb_get_sfml_sprite (mrb, self);

  sfSprite_setTexture (sprite, texture, sfTrue);

  return arg1;
}

static mrb_value
set_position (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  sfVector2f position;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  sprite = mrb_get_sfml_sprite (mrb, self);
  position = mrb_get_sfml_vector_2f (mrb, arg);

  sfSprite_setPosition (sprite, position);
  return arg;
}

static mrb_value
get_position (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  sfVector2f position;

  sprite = mrb_get_sfml_sprite (mrb, self);
  position = sfSprite_getPosition (sprite);

  return wrap_vector_2f (mrb, position);
}

static mrb_value
set_rotation (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  mrb_float angle;

  mrb_get_args (mrb, "f", &angle);
  sprite = mrb_get_sfml_sprite (mrb, self);

  sfSprite_setRotation (sprite, (float) angle);

  return mrb_float_value (mrb, angle);
}

static mrb_value
get_rotation (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  float rotation;

  sprite = mrb_get_sfml_sprite (mrb, self);
  rotation = sfSprite_getRotation (sprite);

  return mrb_float_value (mrb, rotation);
}

static mrb_value
set_scale (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  sfVector2f scale;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  sprite = mrb_get_sfml_sprite (mrb, self);
  scale = mrb_get_sfml_vector_2f (mrb, arg);

  sfSprite_setScale (sprite, scale);
  return arg;
}

static mrb_value
get_scale (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  sfVector2f scale;

  sprite = mrb_get_sfml_sprite (mrb, self);
  scale = sfSprite_getScale (sprite);

  return wrap_vector_2f (mrb, scale);
}

static mrb_value
set_origin (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  sfVector2f origin;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  sprite = mrb_get_sfml_sprite (mrb, self);
  origin = mrb_get_sfml_vector_2f (mrb, arg);

  sfSprite_setOrigin (sprite, origin);
  return arg;
}

static mrb_value
get_origin (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  sfVector2f origin;

  sprite = mrb_get_sfml_sprite (mrb, self);
  origin = sfSprite_getOrigin (sprite);

  return wrap_vector_2f (mrb, origin);
}

static mrb_value
move (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  sfVector2f offset;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  sprite = mrb_get_sfml_sprite (mrb, self);
  offset = mrb_get_sfml_vector_2f (mrb, arg);

  sfSprite_move (sprite, offset);

  return get_position (mrb, self);
}

static mrb_value
rotate (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  mrb_float angle;

  mrb_get_args (mrb, "f", &angle);

  sprite = mrb_get_sfml_sprite (mrb, self);

  sfSprite_rotate (sprite, (float) angle);

  return get_rotation (mrb, self);
}

static mrb_value
resize (mrb_state * mrb, mrb_value self)
{
  sfSprite *sprite;
  sfVector2f scale;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  sprite = mrb_get_sfml_sprite (mrb, self);
  scale = mrb_get_sfml_vector_2f (mrb, arg);

  sfSprite_scale (sprite, scale);

  return get_scale (mrb, self);
}

void
mrb_mruby_sfml_sprite_init (mrb_state * mrb, struct RClass *sfml)
{
  struct RClass *sprite = mrb_define_class_under (mrb, sfml, "Sprite",
                                                  mrb->object_class);

  MRB_SET_INSTANCE_TT (sprite, MRB_TT_DATA);

  mrb_define_method (mrb, sprite, "initialize", initialize, MRB_ARGS_ANY ());
  mrb_define_method (mrb, sprite, "set_texture", set_texture, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, sprite, "position=", set_position,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, sprite, "position", get_position, MRB_ARGS_NONE ());
  mrb_define_method (mrb, sprite, "move", move, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, sprite, "rotation=", set_rotation,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, sprite, "rotation", get_rotation, MRB_ARGS_NONE ());
  mrb_define_method (mrb, sprite, "rotate", rotate, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, sprite, "scale=", set_scale, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, sprite, "scale", get_scale, MRB_ARGS_NONE ());
  mrb_define_method (mrb, sprite, "resize", resize, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, sprite, "origin=", set_origin, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, sprite, "origin", get_origin, MRB_ARGS_NONE ());
}
