#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>

#include <SFML/Graphics.h>

struct RClass *class_Font;

void
font_free (mrb_state * mrb, void *p)
{
  sfFont_destroy (p);
}

static const struct mrb_data_type mrb_sfml_font_type = { "SFML::Font",
  font_free
};

mrb_value
wrap_font (mrb_state * mrb, sfFont * font)
{
  return
    mrb_obj_value (Data_Wrap_Struct
                   (mrb, class_Font, &mrb_sfml_font_type, font));
}

sfFont *
mrb_get_sfml_font (mrb_state * mrb, mrb_value value)
{
  return DATA_GET_PTR (mrb, value, &mrb_sfml_font_type, sfFont);
}

static mrb_value
font_create_from_file (mrb_state * mrb, mrb_value self)
{
  mrb_value filename;

  mrb_get_args (mrb, "S", &filename);

  sfFont *font = sfFont_createFromFile (RSTRING_PTR (filename));

  return mrb_obj_value (Data_Wrap_Struct (mrb, mrb_class_ptr (self),
                                          &mrb_sfml_font_type, font));
}

void
mrb_mruby_sfml_font_init (mrb_state * mrb, struct RClass *sfml)
{
  class_Font = mrb_define_class_under (mrb, sfml, "Font", mrb->object_class);

  MRB_SET_INSTANCE_TT (class_Font, MRB_TT_DATA);

  mrb_define_class_method (mrb, class_Font, "create_from_file",
                           font_create_from_file, MRB_ARGS_REQ (1));
}
