#ifndef _MRB_SFML_TRANSFORM_H_
#define _MRB_SFML_TRANSFORM_H_

#include <mruby.h>

#include <SFML/Graphics/Transform.h>

void mrb_mruby_sfml_transform_init (mrb_state *, struct RClass *);

sfTransform *mrb_get_sfml_transform (mrb_state *, mrb_value);

mrb_value wrap_transform(mrb_state *, sfTransform);

#endif
