#ifndef _MRB_SFML_TEXT_H_
#define _MRB_SFML_TEXT_H_

#include <mruby.h>
#include <mruby/class.h>
#include <SFML/Graphics.h>

void mrb_mruby_sfml_text_init (mrb_state *, struct RClass *);

mrb_value mrb_wrap_text (mrb_state *, sfText *);

sfText *mrb_get_sfml_text (mrb_state *, mrb_value value);

#endif
