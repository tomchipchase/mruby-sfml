#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>

#include <SFML/Graphics.h>

#include "mrb_sfml_video_mode.h"
#include "mrb_sfml_window.h"
#include "mrb_sfml_sprite.h"
#include "mrb_sfml_text.h"
#include "mrb_sfml_event.h"

void
window_free (mrb_state * mrb, void *p)
{
  sfRenderWindow_destroy (p);
}

static const struct mrb_data_type mrb_sfml_window_type = { "SFML::Window",
  window_free
};

sfRenderWindow *
mrb_get_sfml_window (mrb_state * mrb, mrb_value value)
{
  return DATA_GET_PTR (mrb, value, &mrb_sfml_window_type, sfRenderWindow);
}

static mrb_value
window_init (mrb_state * mrb, mrb_value self)
{
  mrb_value arg1, title;
  sfVideoMode *video_mode;

  mrb_get_args (mrb, "oS", &arg1, &title);

  video_mode = mrb_get_sfml_video_mode (mrb, arg1);

  DATA_TYPE (self) = &mrb_sfml_window_type;

  sfContextSettings context_settings = { 32, 32, 0, 1, 0 };
  sfRenderWindow *window = sfRenderWindow_create (*video_mode,
                                                  RSTRING_PTR (title),
                                                  sfDefaultStyle,
                                                  &context_settings);

  DATA_PTR (self) = window;
  return self;
}

static mrb_value
window_is_open (mrb_state * mrb, mrb_value self)
{
  sfRenderWindow *window;

  window = mrb_get_sfml_window (mrb, self);
  return mrb_bool_value (sfRenderWindow_isOpen (window));
}

static mrb_value
window_close (mrb_state * mrb, mrb_value self)
{
  sfRenderWindow *window;

  window = mrb_get_sfml_window (mrb, self);
  sfRenderWindow_close (window);
  return self;
}

static mrb_value
window_draw_sprite (mrb_state * mrb, mrb_value self)
{
  sfRenderWindow *window;
  sfSprite *sprite;

  window = mrb_get_sfml_window (mrb, self);
  mrb_value arg1;

  mrb_get_args (mrb, "o", &arg1);
  sprite = mrb_get_sfml_sprite (mrb, arg1);
  sfRenderWindow_drawSprite (window, sprite, NULL);
  return self;
}

static mrb_value
window_draw_text (mrb_state * mrb, mrb_value self)
{
  sfRenderWindow *window;
  sfText *text;

  window = mrb_get_sfml_window (mrb, self);
  mrb_value arg1;

  mrb_get_args (mrb, "o", &arg1);
  text = mrb_get_sfml_text (mrb, arg1);
  sfRenderWindow_drawText (window, text, NULL);
  return self;
}

static mrb_value
window_clear (mrb_state * mrb, mrb_value self)
{
  sfRenderWindow *window;

  window = mrb_get_sfml_window (mrb, self);
  sfRenderWindow_clear (window, sfBlack);
  return self;
}

static mrb_value
window_display (mrb_state * mrb, mrb_value self)
{
  sfRenderWindow *window;

  window = mrb_get_sfml_window (mrb, self);
  sfRenderWindow_display (window);
  return self;
}

static mrb_value
window_poll_event (mrb_state * mrb, mrb_value self)
{
  sfRenderWindow *window;
  sfEvent event;

  window = mrb_get_sfml_window (mrb, self);

  if (sfRenderWindow_pollEvent (window, &event))
    {
      return mrb_wrap_event (mrb, event);
    }
  else
    {
      return mrb_nil_value ();
    }
}

void
mrb_mruby_sfml_window_init (mrb_state * mrb, struct RClass *sfml)
{
  struct RClass *window = mrb_define_class_under (mrb, sfml, "Window",
                                                  mrb->object_class);

  MRB_SET_INSTANCE_TT (window, MRB_TT_DATA);

  mrb_define_method (mrb, window, "initialize", window_init,
                     MRB_ARGS_REQ (2));
  mrb_define_method (mrb, window, "open?", window_is_open, MRB_ARGS_NONE ());
  mrb_define_method (mrb, window, "close!", window_close, MRB_ARGS_NONE ());
  mrb_define_method (mrb, window, "draw_sprite", window_draw_sprite,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, window, "draw_text", window_draw_text,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, window, "poll_event", window_poll_event,
                     MRB_ARGS_NONE ());
  mrb_define_method (mrb, window, "clear!", window_clear, MRB_ARGS_NONE ());
  mrb_define_method (mrb, window, "display!", window_display,
                     MRB_ARGS_NONE ());
}
