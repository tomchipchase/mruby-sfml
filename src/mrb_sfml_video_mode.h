#ifndef _MRB_SFML_VIDEO_MODE_H_
#define _MRB_SFML_VIDEO_MODE_H_

#include <mruby.h>

void mrb_mruby_sfml_video_mode_init (mrb_state *, struct RClass *);

sfVideoMode *mrb_get_sfml_video_mode (mrb_state *, mrb_value);

#endif
