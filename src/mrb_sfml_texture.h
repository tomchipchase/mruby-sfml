#ifndef _MRB_SFML_TEXTURE_H_
#define _MRB_SFML_TEXTURE_H_

#include <mruby.h>

void mrb_mruby_sfml_texture_init (mrb_state *, struct RClass *);

sfTexture *mrb_get_sfml_texture (mrb_state *, mrb_value);

mrb_value wrap_texture (mrb_state *, const sfTexture *);

#endif
