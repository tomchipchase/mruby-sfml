#ifndef _MRB_SFML_EVENT_H_
#define _MRB_SFML_EVENT_H_

#include <mruby.h>
#include <mruby/class.h>
#include <SFML/Graphics.h>

void mrb_mruby_sfml_event_init (mrb_state *, struct RClass *);

mrb_value mrb_wrap_event (mrb_state *, sfEvent);

sfEvent *mrb_get_sfml_event (mrb_state *, mrb_value value);

#endif
