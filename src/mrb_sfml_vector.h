#ifndef _MRB_SFML_VECTOR_H_
#define _MRB_SFML_VECTOR_H_

#include <mruby.h>
#include <mruby/class.h>
#include <SFML/Graphics.h>

void mrb_mruby_sfml_vector_init (mrb_state *, struct RClass *);

sfVector2i mrb_get_sfml_vector_2i (mrb_state *, mrb_value);
sfVector2u mrb_get_sfml_vector_2u (mrb_state *, mrb_value);
sfVector2f mrb_get_sfml_vector_2f (mrb_state *, mrb_value);
sfVector3f mrb_get_sfml_vector_3f (mrb_state *, mrb_value);

mrb_value wrap_vector_2i (mrb_state *, sfVector2i);
mrb_value wrap_vector_2u (mrb_state *, sfVector2u);
mrb_value wrap_vector_2f (mrb_state *, sfVector2f);
mrb_value wrap_vector_3f (mrb_state *, sfVector3f);

#endif
