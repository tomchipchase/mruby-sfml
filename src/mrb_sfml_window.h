#ifndef _MRB_SFML_WINDOW_H_
#define _MRB_SFML_WINDOW_H_

#include <mruby.h>

void mrb_mruby_sfml_window_init (mrb_state *, struct RClass *);

sfRenderWindow *mrb_get_sfml_window (mrb_state *, mrb_value);

#endif
