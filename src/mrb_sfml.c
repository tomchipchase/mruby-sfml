#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>
#include <stdio.h>

#include <SFML/Graphics.h>

#include "mrb_sfml_sprite.h"
#include "mrb_sfml_video_mode.h"
#include "mrb_sfml_window.h"
#include "mrb_sfml_texture.h"
#include "mrb_sfml_transform.h"
#include "mrb_sfml_event.h"
#include "mrb_sfml_font.h"
#include "mrb_sfml_text.h"
#include "mrb_sfml_music.h"
#include "mrb_sfml_vector.h"

void
mrb_mruby_sfml_gem_init (mrb_state * mrb)
{
  struct RClass *sfml_module = mrb_define_module (mrb, "SFML");

  mrb_mruby_sfml_sprite_init (mrb, sfml_module);
  mrb_mruby_sfml_window_init (mrb, sfml_module);
  mrb_mruby_sfml_video_mode_init (mrb, sfml_module);
  mrb_mruby_sfml_texture_init (mrb, sfml_module);
  mrb_mruby_sfml_event_init (mrb, sfml_module);
  mrb_mruby_sfml_font_init (mrb, sfml_module);
  mrb_mruby_sfml_text_init (mrb, sfml_module);
  mrb_mruby_sfml_music_init (mrb, sfml_module);
  mrb_mruby_sfml_vector_init (mrb, sfml_module);
  mrb_mruby_sfml_transform_init (mrb, sfml_module);
}

void
mrb_mruby_sfml_gem_final (mrb_state * mrb)
{
}
