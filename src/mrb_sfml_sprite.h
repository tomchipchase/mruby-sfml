#ifndef _MRB_SFML_SPRITE_H_
#define _MRB_SFML_SPRITE_H_

#include <mruby.h>

void mrb_mruby_sfml_sprite_init (mrb_state *, struct RClass *);

sfSprite *mrb_get_sfml_sprite (mrb_state *, mrb_value);

#endif
