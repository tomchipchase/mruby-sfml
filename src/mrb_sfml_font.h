
#ifndef _MRB_SFML_FONT_H_
#define _MRB_SFML_FONT_H_

#include <mruby.h>
#include <mruby/class.h>
#include <SFML/Graphics.h>

void mrb_mruby_sfml_font_init (mrb_state *, struct RClass *);

sfFont *mrb_get_sfml_font (mrb_state *, mrb_value value);
mrb_value wrap_font (mrb_state * mrb, sfFont * font);

#endif
