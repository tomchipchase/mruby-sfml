#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>

#include <SFML/Audio.h>

void
music_free (mrb_state * mrb, void *p)
{
  sfMusic_stop (p);
  sfMusic_destroy (p);
}

static const struct mrb_data_type mrb_sfml_music_type = { "SFML::Music",
  music_free
};

sfMusic *
mrb_get_sfml_music (mrb_state * mrb, mrb_value value)
{
  return DATA_GET_PTR (mrb, value, &mrb_sfml_music_type, sfMusic);
}

static mrb_value
music_create_from_file (mrb_state * mrb, mrb_value self)
{
  mrb_value filename;

  mrb_get_args (mrb, "S", &filename);

  sfMusic *music = sfMusic_createFromFile (RSTRING_PTR (filename));

  return mrb_obj_value (Data_Wrap_Struct (mrb, mrb_class_ptr (self),
                                          &mrb_sfml_music_type, music));
}

static mrb_value
music_play (mrb_state * mrb, mrb_value self)
{
  sfMusic *music = mrb_get_sfml_music (mrb, self);

  sfMusic_play (music);

  return self;
}

void
mrb_mruby_sfml_music_init (mrb_state * mrb, struct RClass *sfml)
{
  struct RClass *music = mrb_define_class_under (mrb, sfml, "Music",
                                                 mrb->object_class);

  MRB_SET_INSTANCE_TT (music, MRB_TT_DATA);

  mrb_define_class_method (mrb, music, "create_from_file",
                           music_create_from_file, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, music, "play", music_play, MRB_ARGS_NONE ());
}
