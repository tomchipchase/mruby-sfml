#include <string.h>
#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>

#include <SFML/Graphics.h>

#include "mrb_sfml_event.h"

static struct RClass *module_Event;

#define CLASS(name) static struct RClass *class_##name;\
                    static const struct mrb_data_type name##_type = { \
                      "SFML::Event::" #name, mrb_free }

CLASS(Base);
CLASS(Key);
CLASS(MouseButton);
CLASS(JoystickButton);
CLASS(JoystickConnect);

CLASS(Closed);
CLASS(Resized);
CLASS(LostFocus);
CLASS(GainedFocus);
CLASS(TextEntered);
CLASS(KeyPressed);
CLASS(KeyReleased);
CLASS(MouseWheelMoved);
CLASS(MouseButtonPressed);
CLASS(MouseButtonReleased);
CLASS(MouseMoved);
CLASS(MouseEntered);
CLASS(MouseLeft);
CLASS(JoystickButtonPressed);
CLASS(JoystickButtonReleased);
CLASS(JoystickMoved);
CLASS(JoystickConnected);
CLASS(JoystickDisconnected);

#undef CLASS

sfEvent *
mrb_get_sfml_event (mrb_state * mrb, mrb_value value)
{
#define TRY(name) if (DATA_TYPE(value) == &name##_type) \
                    return DATA_GET_PTR (mrb, value, &name##_type, sfEvent)
  TRY(Closed);
  TRY(Resized);
  TRY(LostFocus);
  TRY(GainedFocus);
  TRY(TextEntered);
  TRY(KeyPressed);
  TRY(KeyReleased);
  TRY(MouseWheelMoved);
  TRY(MouseButtonPressed);
  TRY(MouseButtonReleased);
  TRY(MouseMoved);
  TRY(MouseEntered);
  TRY(MouseLeft);
  TRY(JoystickButtonPressed);
  TRY(JoystickButtonReleased);
  TRY(JoystickMoved);
  TRY(JoystickConnected);
  TRY(JoystickDisconnected);
#undef TRY
  return NULL;
}

mrb_value
mrb_wrap_event (mrb_state * mrb, sfEvent event)
{
  sfEvent *new_event;

  new_event = mrb_malloc (mrb, sizeof (*new_event));
  memcpy (new_event, &event, sizeof (event));


  switch (new_event->type)
  {
#define CASE(name)  case(sfEvt##name):\
          return mrb_obj_value(Data_Wrap_Struct(mrb, class_##name,\
                               &name##_type,new_event))
    CASE(Closed);
    CASE(Resized);
    CASE(LostFocus);
    CASE(GainedFocus);
    CASE(TextEntered);
    CASE(KeyPressed);
    CASE(KeyReleased);
    CASE(MouseWheelMoved);
    CASE(MouseButtonPressed);
    CASE(MouseButtonReleased);
    CASE(MouseMoved);
    CASE(MouseEntered);
    CASE(MouseLeft);
    CASE(JoystickButtonPressed);
    CASE(JoystickButtonReleased);
    CASE(JoystickMoved);
    CASE(JoystickConnected);
    CASE(JoystickDisconnected);

#undef CASE

    default: return mrb_nil_value ();
  }

}

static mrb_value
event_Key_code (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value (event->key.code);
}

static mrb_value
event_Key_alt (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_bool_value (event->key.alt);
}

static mrb_value
event_Key_control (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_bool_value (event->key.control);
}

static mrb_value
event_Key_system (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_bool_value (event->key.system);
}

static mrb_value
event_Key_shift (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_bool_value (event->key.shift);
}

static mrb_value
event_Resized_width (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value (event->size.width);
}

static mrb_value
event_Resized_height (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value (event->size.height);
}

static mrb_value
event_TextEntered_unicode (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  // TODO: convert sfUint32 to ruby char
  return mrb_ptr_to_str (mrb, event->text.unicode);
}

static mrb_value
event_MouseMoved_x (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->mouseMove.x);
}

static mrb_value
event_MouseMoved_y (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->mouseMove.y);
}

static mrb_value
event_MouseButton_x (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->mouseButton.x);
}

static mrb_value
event_MouseButton_y (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->mouseButton.y);
}

static mrb_value
event_MouseButton_button (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->mouseButton.button);
}

static mrb_value
event_MouseWheelMoved_x (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->mouseWheel.x);
}

static mrb_value
event_MouseWheelMoved_y (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->mouseWheel.y);
}

static mrb_value
event_MouseWheelMoved_delta (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->mouseWheel.delta);
}

static mrb_value
event_JoystickMoved_id (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->joystickMove.joystickId);
}

static mrb_value
event_JoystickMoved_axis (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->joystickMove.axis);
}

static mrb_value
event_JoystickMoved_position (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_float_value(mrb, event->joystickMove.position);
}

static mrb_value
event_JoystickButton_id (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->joystickButton.joystickId);
}

static mrb_value
event_JoystickButton_button (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->joystickButton.button);
}

static mrb_value
event_JoystickConnect_id (mrb_state *mrb, mrb_value self) {
  sfEvent *event;
  event = mrb_get_sfml_event (mrb, self);
  return mrb_fixnum_value(event->joystickConnect.joystickId);
}

void
mrb_mruby_sfml_event_init (mrb_state * mrb, struct RClass *sfml)
{
  module_Event = mrb_define_module_under (mrb, sfml, "Event");

  class_Base = mrb_define_class_under(mrb, module_Event, "Base",
                                      mrb->object_class);
  MRB_SET_INSTANCE_TT(class_Base, MRB_TT_DATA);

#define EVENT(name, super) class_##name = \
          mrb_define_class_under(mrb, module_Event, #name,\
                                 class_##super);\
          MRB_SET_INSTANCE_TT(class##_##name, MRB_TT_DATA)

  EVENT(Key, Base);
  EVENT(MouseButton, Base);
  EVENT(JoystickButton, Base);
  EVENT(JoystickConnect, Base);

  EVENT(Closed, Base);
  EVENT(Resized, Base);
  EVENT(LostFocus, Base);
  EVENT(GainedFocus, Base);
  EVENT(TextEntered, Base);
  EVENT(KeyPressed, Key);
  EVENT(KeyReleased, Key);
  EVENT(MouseWheelMoved, Base);
  EVENT(MouseButtonPressed, MouseButton);
  EVENT(MouseButtonReleased, MouseButton);
  EVENT(MouseMoved, Base);
  EVENT(MouseEntered, Base);
  EVENT(MouseLeft, Base);
  EVENT(JoystickButtonPressed, JoystickButton);
  EVENT(JoystickButtonReleased, JoystickButton);
  EVENT(JoystickMoved, Base);
  EVENT(JoystickConnected, JoystickConnect);
  EVENT(JoystickDisconnected, JoystickConnect);

#undef EVENT

#define DEFINE_METHOD(class, name) \
  mrb_define_method (mrb, class_##class, #name, event_##class##_##name,\
                     MRB_ARGS_NONE())

  DEFINE_METHOD (Resized, width);
  DEFINE_METHOD (Resized, height);
  DEFINE_METHOD (Key, code);
  DEFINE_METHOD (Key, alt);
  DEFINE_METHOD (Key, control);
  DEFINE_METHOD (Key, shift);
  DEFINE_METHOD (Key, system);
  DEFINE_METHOD (TextEntered, unicode);
  DEFINE_METHOD (MouseMoved, x);
  DEFINE_METHOD (MouseMoved, y);
  DEFINE_METHOD (MouseButton, x);
  DEFINE_METHOD (MouseButton, y);
  DEFINE_METHOD (MouseButton, button);
  DEFINE_METHOD (MouseWheelMoved, x);
  DEFINE_METHOD (MouseWheelMoved, y);
  DEFINE_METHOD (MouseWheelMoved, delta);
  DEFINE_METHOD (JoystickMoved, id);
  DEFINE_METHOD (JoystickMoved, axis);
  DEFINE_METHOD (JoystickMoved, position);
  DEFINE_METHOD (JoystickButton, id);
  DEFINE_METHOD (JoystickButton, button);
  DEFINE_METHOD (JoystickConnect, id);
#undef DEFINE_METHOD
}
