#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>

#include "SFML/Graphics.h"

#include "mrb_sfml_video_mode.h"

static const struct mrb_data_type mrb_sfml_video_mode_type =
  { "SFML::VideoMode", mrb_free };

sfVideoMode *
mrb_get_sfml_video_mode (mrb_state * mrb, mrb_value value)
{
  return DATA_GET_PTR (mrb, value, &mrb_sfml_video_mode_type, sfVideoMode);
}

static mrb_value
video_mode_init (mrb_state * mrb, mrb_value self)
{
  mrb_int w, h;

  mrb_get_args (mrb, "ii", &w, &h);

  sfVideoMode *video_mode;

  video_mode = mrb_malloc (mrb, sizeof (*video_mode));

  video_mode->width = w;
  video_mode->height = h;
  video_mode->bitsPerPixel = 32;

  DATA_TYPE (self) = &mrb_sfml_video_mode_type;
  DATA_PTR (self) = video_mode;
  return self;
}

void
mrb_mruby_sfml_video_mode_init (mrb_state * mrb, struct RClass *sfml)
{
  struct RClass *video_mode = mrb_define_class_under (mrb, sfml, "VideoMode",
                                                      mrb->object_class);

  MRB_SET_INSTANCE_TT (video_mode, MRB_TT_DATA);

  mrb_define_method (mrb, video_mode, "initialize", video_mode_init,
                     MRB_ARGS_REQ (2));
}
