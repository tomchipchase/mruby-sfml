#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>

#include <SFML/Graphics.h>

static struct RClass *class_Texture;

void
texture_free (mrb_state * mrb, void *p)
{
  sfTexture_destroy (p);
}

static const struct mrb_data_type mrb_sfml_texture_type = { "SFML::Texture",
  texture_free
};

sfTexture *
mrb_get_sfml_texture (mrb_state * mrb, mrb_value value)
{
  return DATA_GET_PTR (mrb, value, &mrb_sfml_texture_type, sfTexture);
}

mrb_value
wrap_texture (mrb_state * mrb, sfTexture * texture)
{
  return mrb_obj_value (Data_Wrap_Struct (mrb, class_Texture,
                                          &mrb_sfml_texture_type, texture));
}

static mrb_value
texture_create (mrb_state * mrb, mrb_value self)
{
  unsigned int x, y;

  mrb_get_args (mrb, "ii", &x, &y);

  sfTexture *texture = sfTexture_create (x, y);

  return wrap_texture (mrb, texture);
}

static mrb_value
texture_create_from_file (mrb_state * mrb, mrb_value self)
{
  mrb_value filename;

  mrb_get_args (mrb, "S", &filename);

  sfTexture *texture =
    sfTexture_createFromFile (RSTRING_PTR (filename), NULL);

  return wrap_texture (mrb, texture);
}

void
mrb_mruby_sfml_texture_init (mrb_state * mrb, struct RClass *sfml)
{
  class_Texture = mrb_define_class_under (mrb, sfml, "Texture",
                                          mrb->object_class);

  MRB_SET_INSTANCE_TT (class_Texture, MRB_TT_DATA);

  mrb_define_class_method (mrb, class_Texture, "create_from_file",
                           texture_create_from_file, MRB_ARGS_REQ (1));
  mrb_define_class_method (mrb, class_Texture, "create",
                           texture_create, MRB_ARGS_REQ (2));
}
