#include <mruby.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/string.h>

#include <SFML/Graphics.h>

#include "mrb_sfml_text.h"
#include "mrb_sfml_font.h"
#include "mrb_sfml_vector.h"

static struct RClass *class_Text;

void
text_free (mrb_state * mrb, void *p)
{
  sfText_destroy (p);
}

static const struct mrb_data_type mrb_sfml_text_type = {
  "SFML::Text", text_free
};

sfText *
mrb_get_sfml_text (mrb_state * mrb, mrb_value value)
{
  return DATA_GET_PTR (mrb, value, &mrb_sfml_text_type, sfText);
}

static mrb_value
initialize (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  mrb_value arg;
  size_t argc;

  argc = mrb_get_args (mrb, "|o", &arg);

  switch (argc)
    {
    case (0):
      text = sfText_create ();
      break;
    case (1):
      text = sfText_copy (mrb_get_sfml_text (mrb, arg));
      break;
    default:
      mrb_raise (mrb, E_ARGUMENT_ERROR, "wrong number of arguments");
      break;
    }

  DATA_TYPE (self) = &mrb_sfml_text_type;
  DATA_PTR (self) = text;
  return self;
}

static mrb_value
set_position (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfVector2f position;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  text = mrb_get_sfml_text (mrb, self);
  position = mrb_get_sfml_vector_2f (mrb, arg);

  sfText_setPosition (text, position);

  return arg;
}

static mrb_value
get_position (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfVector2f position;

  text = mrb_get_sfml_text (mrb, self);
  position = sfText_getPosition (text);

  return wrap_vector_2f (mrb, position);
}

static mrb_value
move (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfVector2f offset;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  text = mrb_get_sfml_text (mrb, self);
  offset = mrb_get_sfml_vector_2f (mrb, arg);

  sfText_move (text, offset);

  return get_position (mrb, self);
}

static mrb_value
set_rotation (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  mrb_float angle;

  mrb_get_args (mrb, "f", &angle);
  text = mrb_get_sfml_text (mrb, self);

  sfText_setRotation (text, (float) angle);

  return mrb_float_value (mrb, angle);
}

static mrb_value
get_rotation (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  float rotation;

  text = mrb_get_sfml_text (mrb, self);
  rotation = sfText_getRotation (text);

  return mrb_float_value (mrb, rotation);
}

static mrb_value
rotate (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  float angle;

  mrb_get_args (mrb, "f", &angle);

  text = mrb_get_sfml_text (mrb, self);

  sfText_rotate (text, (float) angle);

  return get_rotation (mrb, self);
}

static mrb_value
set_scale (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfVector2f scale;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  text = mrb_get_sfml_text (mrb, self);
  scale = mrb_get_sfml_vector_2f (mrb, arg);

  sfText_setScale (text, scale);
  return arg;
}

static mrb_value
get_scale (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfVector2f scale;

  text = mrb_get_sfml_text (mrb, self);
  scale = sfText_getScale (text);

  return wrap_vector_2f (mrb, scale);
}

static mrb_value
resize (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfVector2f scale;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  text = mrb_get_sfml_text (mrb, self);
  scale = mrb_get_sfml_vector_2f (mrb, arg);

  sfText_scale (text, scale);

  return get_scale (mrb, self);
}

static mrb_value
set_origin (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfVector2f origin;
  mrb_value arg;

  mrb_get_args (mrb, "o", &arg);

  text = mrb_get_sfml_text (mrb, self);
  origin = mrb_get_sfml_vector_2f (mrb, arg);

  sfText_setOrigin (text, origin);
  return arg;
}

static mrb_value
get_origin (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfVector2f origin;

  text = mrb_get_sfml_text (mrb, self);
  origin = sfText_getOrigin (text);

  return wrap_vector_2f (mrb, origin);
}

static mrb_value
set_string (mrb_state * mrb, mrb_value self)
{
  mrb_value string;
  sfText *text;

  mrb_get_args (mrb, "S", &string);
  text = mrb_get_sfml_text (mrb, self);

  sfText_setString (text, RSTRING_PTR (string));

  return string;
}

static mrb_value
get_string (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  char *string;

  text = mrb_get_sfml_text (mrb, self);
  string = sfText_getString (text);

  return mrb_ptr_to_str (mrb, string);
}

static mrb_value
set_font (mrb_state * mrb, mrb_value self)
{
  mrb_value arg1;
  sfText *text;
  sfFont *font;

  mrb_get_args (mrb, "o", &arg1);
  text = mrb_get_sfml_text (mrb, self);
  font = mrb_get_sfml_font (mrb, arg1);

  sfText_setFont (text, font);

  return arg1;
}

static mrb_value
get_font (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfFont *font;

  text = mrb_get_sfml_text (mrb, self);
  font = sfText_getFont (text);

  return wrap_font (mrb, font);
}

static mrb_value
reset_style (mrb_state * mrb, mrb_value self)
{
  sfText *text;

  text = mrb_get_sfml_text (mrb, self);
  sfText_setStyle (text, sfTextRegular);
  return self;
}

static mrb_value
underline (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfUint32 current_style;

  text = mrb_get_sfml_text (mrb, self);
  current_style = sfText_getStyle (text);
  sfText_setStyle (text, current_style | sfTextUnderlined);
  return self;
}

static mrb_value
is_underlined (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfUint32 current_style;

  text = mrb_get_sfml_text (mrb, self);
  current_style = sfText_getStyle (text);
  return mrb_bool_value (current_style & sfTextUnderlined);
}

static mrb_value
make_italic (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfUint32 current_style;

  text = mrb_get_sfml_text (mrb, self);
  current_style = sfText_getStyle (text);
  sfText_setStyle (text, current_style | sfTextItalic);
  return self;
}

static mrb_value
is_italic (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfUint32 current_style;

  text = mrb_get_sfml_text (mrb, self);
  current_style = sfText_getStyle (text);
  return mrb_bool_value (current_style & sfTextItalic);
}

static mrb_value
make_bold (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfUint32 current_style;

  text = mrb_get_sfml_text (mrb, self);
  current_style = sfText_getStyle (text);
  sfText_setStyle (text, current_style | sfTextBold);
  return self;
}

static mrb_value
is_bold (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  sfUint32 current_style;

  text = mrb_get_sfml_text (mrb, self);
  current_style = sfText_getStyle (text);
  return mrb_bool_value (current_style & sfTextBold);
}

static mrb_value
set_char_size (mrb_state * mrb, mrb_value self)
{
  mrb_int size;
  sfText *text;

  mrb_get_args (mrb, "i", &size);
  text = mrb_get_sfml_text (mrb, self);

  sfText_setCharacterSize (text, size);

  return mrb_fixnum_value (size);
}

static mrb_value
get_char_size (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  unsigned int size;

  text = mrb_get_sfml_text (mrb, self);
  size = sfText_getCharacterSize (text);

  return mrb_fixnum_value (size);
}

static mrb_value
find_char_position (mrb_state * mrb, mrb_value self)
{
  sfText *text;
  mrb_int index;
  sfVector2f position;

  mrb_get_args (mrb, "i", &index);
  text = mrb_get_sfml_text (mrb, self);

  position = sfText_findCharacterPos (text, index);

  return wrap_vector_2f (mrb, position);
}

void
mrb_mruby_sfml_text_init (mrb_state * mrb, struct RClass *sfml)
{
  class_Text = mrb_define_class_under (mrb, sfml, "Text", mrb->object_class);

  MRB_SET_INSTANCE_TT (class_Text, MRB_TT_DATA);

  mrb_define_method (mrb, class_Text, "initialize", initialize,
                     MRB_ARGS_ANY ());
  mrb_define_method (mrb, class_Text, "position=", set_position,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "position", get_position,
                     MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "move", move, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "rotation=", set_rotation,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "rotation", get_rotation,
                     MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "rotate", rotate, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "scale=", set_scale, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "scale", get_scale, MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "resize", resize, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "origin=", set_origin,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "origin", get_origin, MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "string=", set_string,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "string", get_string, MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "font=", set_font, MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "font", get_font, MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "char_size=", set_char_size,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "char_size", get_char_size,
                     MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "char_pos", find_char_position,
                     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, class_Text, "reset_style", reset_style,
                     MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "underline", underline,
                     MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "underlined?", is_underlined,
                     MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "embolden", make_bold,
                     MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "bold?", is_bold, MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "italicise", make_italic,
                     MRB_ARGS_NONE ());
  mrb_define_method (mrb, class_Text, "italic?", is_italic, MRB_ARGS_NONE ());
}
